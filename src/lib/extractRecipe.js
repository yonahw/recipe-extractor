import { API_KEY } from '$env/static/private';
import { fail } from '@sveltejs/kit';

const BASE_URL = `https://api.spoonacular.com/recipes/extract?apiKey=${API_KEY}&url=`;

const extractRecipe = async (url) => {
	const request = await fetch(`${BASE_URL}${url}`);
	const data = await request.json();
	// Pages without a recipe will return data but won't have any instructions
	if (!data || !data.instructions) {
		return fail(422, { error: 'The recipe could not be extracted, please try a different URL' });
	}
	return data;
};

export { extractRecipe };
