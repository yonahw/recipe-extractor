import { extractRecipe } from '$lib/extractRecipe';

export const actions = {
	default: async ({ request }) => {
		const data = await request.formData();
		const url = data.get('url');
		return await extractRecipe(url);
	}
};
