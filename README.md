# Recipe Extractor

A web app which extracts recipes from blogs or other sites around the web containing recipes

## TODO

- Add tests surrounding recipe retrieval and parsing
- Flatten recipe object returned from API
- Add recipe Source to page
- Add backend with user features to save recipes
- Add recipe search to backend
- Add tags (investigate why API doesn't return accurate data on vegan, dairyFree, etc)
